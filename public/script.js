document.addEventListener("DOMContentLoaded", function() {
    let image = document.getElementById("burndownChart");

    image.addEventListener("click", function() {
        if (image.classList.contains("hacerZoom")) {
            image.classList.remove("hacerZoom");
        } else {
            image.classList.add("hacerZoom");
        }
    });
});